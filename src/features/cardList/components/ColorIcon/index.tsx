import { StaticImageData } from 'next/image';
import { FC, useEffect, useState } from 'react';

import { useRecoilState } from 'recoil';

import Presenter from './Presenter';
import SelectedColorStateAtom from '@recoil/atoms/SelectedColorStateAtom';
import { ManaColor } from '@types';

interface ColorIconProps {
  color?: ManaColor;
  colorImage: StaticImageData;
}

const ColorIcon: FC<ColorIconProps> = ({ color, colorImage }) => {
  const [selectedColor, setSelectedColor] = useRecoilState<ManaColor | undefined>(
    SelectedColorStateAtom,
  );
  const [isHover, setIsHover] = useState<boolean>(false);
  const [bgColorClass, setBgColorClass] = useState<string>('bg-white');
  const [afterBgColorClass, setAfterBgColorClass] = useState<string>('after:bg-white');

  useEffect(() => {
    switch (color) {
      case ManaColor.W:
        setBgColorClass('bg-white');
        setAfterBgColorClass('after:bg-white');
        break;
      case ManaColor.U:
        setBgColorClass('bg-blue-500');
        setAfterBgColorClass('after:bg-blue-500');
        break;
      case ManaColor.B:
        setBgColorClass('bg-black');
        setAfterBgColorClass('after:bg-black');
        break;
      case ManaColor.R:
        setBgColorClass('bg-red-500');
        setAfterBgColorClass('after:bg-red-500');
        break;
      case ManaColor.G:
        setBgColorClass('bg-green-500');
        setAfterBgColorClass('after:bg-green-500');
        break;
      case ManaColor.C:
        setBgColorClass('bg-gray-500');
        setAfterBgColorClass('after:bg-gray-500');
        break;
      default:
        setBgColorClass('bg-gradient-to-br from-amber-600 to-red-500');
        setAfterBgColorClass('after:bg-gradient-to-br after:from-amber-600 after:to-red-500');
        break;
    }
  }, [color]);

  return (
    <Presenter
      isSelected={selectedColor === color}
      setSelectedColor={setSelectedColor}
      color={color}
      isHover={isHover}
      setIsHover={setIsHover}
      colorImage={colorImage}
      bgColorClass={bgColorClass}
      afterBgColorClass={afterBgColorClass}
    />
  );
};

export default ColorIcon;
