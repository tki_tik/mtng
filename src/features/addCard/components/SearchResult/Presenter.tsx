import { Dispatch, FC, OptionHTMLAttributes, SetStateAction } from 'react';

import classnames from 'classnames';

import Button from '@components/Button';
import CardInfo from '@components/CardInfo';
import Select from '@components/Select';
import Text from '@components/Text';
import { PossessionCard } from '@types';

interface Props {
  isCardInfoEdit: boolean;
  setIsCardInfoEdit: Dispatch<SetStateAction<boolean>>;
  possession: string;
  card: PossessionCard;
  setCard: Dispatch<SetStateAction<PossessionCard>>;
  foilOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>>;
  foilType: string;
  setFoilType: Dispatch<SetStateAction<string>>;
  number: string;
  setNumber: Dispatch<SetStateAction<string>>;
  onRegistrationClick: () => void;
  isDisabledRegistration: boolean;
}

const Presenter: FC<Props> = ({
  isCardInfoEdit,
  setIsCardInfoEdit,
  possession,
  card,
  setCard,
  foilOptionList,
  foilType,
  setFoilType,
  number,
  setNumber,
  onRegistrationClick,
  isDisabledRegistration,
}) => {
  return (
    <>
      <div className={classnames('flex-grow', 'w-full', 'py-6', 'border-b', 'border-gray-300')}>
        <CardInfo
          isEdit={isCardInfoEdit}
          setIsEdit={setIsCardInfoEdit}
          card={card}
          setCard={setCard}
        />
      </div>
      <div className={classnames('flex', 'justify-end', 'gap-4', 'w-full', 'h-12')}>
        <div className={classnames('h-full', 'bg-white')}>
          <Select optionList={foilOptionList} setValue={setFoilType} value={foilType} />
        </div>
        {Number(possession) > 0 && (
          <div className={classnames('w-20')}>
            <Text label={'所持枚数'} value={possession} type={'number'} readOnly />
          </div>
        )}
        <div className={classnames('w-20')}>
          <Text label={'追加枚数'} setValue={setNumber} value={number} type={'number'} />
        </div>
        <div className={classnames('w-24', 'h-full')}>
          <Button disabled={isDisabledRegistration} onClick={onRegistrationClick}>
            <span>{Number(possession) > 0 ? '追加' : '登録'}</span>
          </Button>
        </div>
      </div>
    </>
  );
};

export default Presenter;
