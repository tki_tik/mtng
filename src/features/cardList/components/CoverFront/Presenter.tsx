import NextImage, { StaticImageData } from 'next/image';
import { Dispatch, FC, SetStateAction } from 'react';

import classnames from 'classnames';

import LeftPageLayout from '@features/cardList/components/LeftPageLayout';
import RightPageLayout from '@features/cardList/components/RightPageLayout';

interface Props {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  zIndex: number;
  bgColorClass: string;
  borderColorClass: string;
  colorLogo: StaticImageData;
}

const Presenter: FC<Props> = ({
  isOpen,
  setIsOpen,
  zIndex,
  bgColorClass,
  borderColorClass,
  colorLogo,
}) => {
  return (
    <label>
      <input
        className={classnames('hidden')}
        type="checkbox"
        checked={isOpen}
        onChange={() => setIsOpen(!isOpen)}
      />
      <RightPageLayout isOpen={isOpen} coverColorClass={bgColorClass} zIndex={zIndex}>
        <div
          className={classnames(
            'flex',
            'justify-center',
            'items-center',
            'w-full',
            'h-full',
            'border',
            'border-dashed',
            borderColorClass,
          )}
        >
          <NextImage className={classnames('w-1/3')} src={colorLogo} alt="type_image" />
        </div>
      </RightPageLayout>
      <LeftPageLayout isOpen={isOpen} coverColorClass={bgColorClass}>
        <div
          className={classnames('w-full', 'h-full', 'border', 'border-dashed', borderColorClass)}
        />
      </LeftPageLayout>
    </label>
  );
};

export default Presenter;
