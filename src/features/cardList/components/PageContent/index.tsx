import { FC, MouseEvent, useEffect, useState } from 'react';

import Presenter from './Presenter';
import { PossessionCard } from '@types';

interface PageContentProps {
  cardList: Array<PossessionCard>;
}

const PageContent: FC<PageContentProps> = ({ cardList }) => {
  const [activeCard, setActiveCard] = useState<PossessionCard>();
  const [isDispCardInfoModal, setIsDispCardInfoModal] = useState<boolean>(false);

  const onCardClick = async (
    e: MouseEvent<HTMLDivElement, globalThis.MouseEvent>,
    index: number,
  ) => {
    e.preventDefault();
    setActiveCard(cardList[index]);
    setIsDispCardInfoModal(true);
  };

  useEffect(() => {
    activeCard && setIsDispCardInfoModal(true);
  }, [activeCard]);

  useEffect(() => {
    !isDispCardInfoModal && setActiveCard(undefined);
  }, [isDispCardInfoModal]);

  return (
    <Presenter
      cardList={cardList}
      isDispCardInfoModal={isDispCardInfoModal}
      setIsDispCardInfoModal={setIsDispCardInfoModal}
      activeCard={activeCard}
      onCardClick={onCardClick}
    />
  );
};

export default PageContent;
