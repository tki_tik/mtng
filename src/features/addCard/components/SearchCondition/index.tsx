import {
  Dispatch,
  FC,
  OptionHTMLAttributes,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';

import { useRecoilState, useSetRecoilState } from 'recoil';

import Presenter from './Presenter';
import { ToastInfo } from '@components/Toast';
import { LANG_TYPE } from '@constants';
import scryfallCardsCodeNumberLang from '@features/addCard/api/scryfallCards';
import { Card } from '@features/addCard/types';
import AllCardStateAtom, { AllCardState } from '@recoil/atoms/AllCardStateAtom';
import LoadingStateAtom from '@recoil/atoms/LoadingStateAtom';
import ShowToastStateAtom from '@recoil/atoms/ShowToastStateAtom';
import { MessageLevel } from '@types';

interface SearchProps {
  setCard: Dispatch<SetStateAction<Card | undefined>>;
}

const SearchCondition: FC<SearchProps> = ({ setCard }) => {
  const setLoading = useSetRecoilState<boolean>(LoadingStateAtom);
  const setToastInfo = useSetRecoilState<ToastInfo>(ShowToastStateAtom);
  const [allCards, setAllCards] = useRecoilState<AllCardState>(AllCardStateAtom);
  const [set, setSet] = useState<string>('');
  const [lang, setLang] = useState<string>(LANG_TYPE.JP);
  const [number, setNumber] = useState<string>('');

  const langOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>> = Object.keys(LANG_TYPE).map(
    (key) => {
      return { label: key, value: LANG_TYPE[key] };
    },
  );

  const init = useCallback(async () => {
    setLoading(true);
    const res = await fetch('/api/possessionCard');
    const data = await res.json();
    setAllCards({ acquired: true, data });
    setLoading(false);
  }, [setAllCards, setLoading]);

  const onSearchClick = async () => {
    if (!set) {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: 'Set を入力してください',
      });
      return;
    }
    if (!number) {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: 'Number を入力してください',
      });
      return;
    }
    setLoading(true);
    const res = await scryfallCardsCodeNumberLang({ set, lang, number });
    if (res.object === 'card') {
      setToastInfo({
        level: MessageLevel.NONE,
        message: '',
      });
      setCard(res);
    } else {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: '検索結果が存在しません',
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    !allCards.acquired && init();
  }, [init, allCards]);

  return (
    <Presenter
      set={set}
      setSet={setSet}
      langOptionList={langOptionList}
      lang={lang}
      setLang={setLang}
      number={number}
      setNumber={setNumber}
      onSearchClick={onSearchClick}
    />
  );
};

export default SearchCondition;
