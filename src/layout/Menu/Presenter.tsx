import Link from 'next/link';
import { FC } from 'react';

import classnames from 'classnames';
import { IconType } from 'react-icons';

interface Props {
  path: string;
  label: string;
  Icon: IconType;
  isDispLabel?: boolean;
  currentPath: string;
}

const Presenter: FC<Props> = ({ path, label, Icon, isDispLabel, currentPath }) => {
  return (
    <Link className={classnames('mt-4')} href={path}>
      <span
        className={classnames(
          'relative',
          'flex',
          'items-center',
          'h-10',
          'my-4',
          'mx-7',
          currentPath === path
            ? ['text-white', 'cursor-default', 'border-b-2', 'border-white']
            : ['text-gray-500', 'cursor-pointer'],
          'hover:text-white',
        )}
      >
        <div className={classnames('absolute', 'pr-3', 'z-10')}>
          <Icon size={24} />
        </div>
        {isDispLabel && (
          <p
            className={classnames(
              'w-full',
              'mx-9',
              'font-bold',
              'text-center',
              'leading-6',
              'whitespace-nowrap',
              'overflow-hidden',
            )}
          >
            {label}
          </p>
        )}
      </span>
    </Link>
  );
};

export default Presenter;
