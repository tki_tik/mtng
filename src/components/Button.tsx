import { ButtonHTMLAttributes, FC, useState } from 'react';

import classnames from 'classnames';

const Button: FC<ButtonHTMLAttributes<HTMLButtonElement>> = ({ children, onClick, disabled }) => {
  const [isHover, setIsHover] = useState<boolean>(false);

  return (
    <button
      className={classnames(
        'relative',
        'flex',
        'justify-center',
        'items-center',
        'w-full',
        'h-full',
        'border-2',
        'text-center',
        'z-[1]',
        'overflow-hidden',
        'outline-none',
        'after:absolute',
        'after:inset-0',
        'after:w-full',
        'after:h-full',
        'after:content-[""]',
        'after:bg-gradient-to-br',
        'after:from-amber-600',
        'after:to-red-500',
        'after:origin-top-left',
        'after:ease-in-out',
        'after:duration-300',
        'after:z-[-1]',
        isHover ? ['text-white', 'after:scale-100'] : ['after:scale-x-0', 'after:scale-y-100'],
        disabled
          ? ['bg-gray-300', 'text-white', 'border-gray-300']
          : ['bg-white', 'text-orange-600', 'border-amber-600'],
      )}
      disabled={disabled}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
