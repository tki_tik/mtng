import { GetStaticProps, NextPage } from 'next';
import { useEffect, useState } from 'react';

import classnames from 'classnames';
import { useRecoilState } from 'recoil';

import SearchCondition from '@features/addCard/components/SearchCondition';
import SearchResult from '@features/addCard/components/SearchResult';
import { Card } from '@features/addCard/types';
import scryfallSymbology from '@features/cardList/api/scryfallSymbology';
import SymbologyStateAtom from '@recoil/atoms/SymbologyStateAtom';
import { Symbology } from '@types';

interface PageProps {
  symbology: Array<Symbology>;
}

const AddCard: NextPage<PageProps> = (props) => {
  const [symbology, setSymbology] = useRecoilState<Array<Symbology>>(SymbologyStateAtom);
  const [card, setCard] = useState<Card>();

  useEffect(() => {
    symbology.length === 0 && setSymbology(props.symbology);
  }, [props.symbology, setSymbology, symbology]);

  return (
    <div className={classnames('flex', 'flex-col', 'gap-4', 'w-4/5', 'h-full', 'p-14')}>
      <div
        className={classnames(
          'flex',
          'flex-col',
          'items-center',
          'gap-4',
          'w-full',
          'h-full',
          'px-16',
          'py-6',
          'rounded',
          'bg-[url("/img/bg-image.jpg")]',
        )}
      >
        <SearchCondition setCard={setCard} />
        {card && <SearchResult card={card} setCard={setCard} />}
      </div>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const { data: symbology } = await scryfallSymbology();

  return {
    props: {
      title: 'カード追加',
      symbology,
    },
  };
};

export default AddCard;
