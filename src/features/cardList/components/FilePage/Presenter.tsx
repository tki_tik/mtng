import { Dispatch, FC, SetStateAction } from 'react';

import classnames from 'classnames';

import LeftPageLayout from '@features/cardList/components/LeftPageLayout';
import PageContent from '@features/cardList/components/PageContent';
import RightPageLayout from '@features/cardList/components/RightPageLayout';
import { PossessionCard } from '@types';

interface Props {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  zIndex: number;
  cardList: Array<PossessionCard>;
}

const Presenter: FC<Props> = ({ isOpen, setIsOpen, zIndex, cardList }) => {
  return (
    <label>
      <input
        className={classnames('hidden')}
        type="checkbox"
        checked={isOpen}
        onChange={() => setIsOpen(!isOpen)}
      />
      <RightPageLayout isOpen={isOpen} zIndex={zIndex}>
        <PageContent cardList={cardList.slice(0, 9)} />
      </RightPageLayout>
      <LeftPageLayout isOpen={isOpen}>
        <PageContent cardList={cardList.slice(9, 18)} />
      </LeftPageLayout>
    </label>
  );
};

export default Presenter;
