import { Head, Html, Main, NextScript } from 'next/document';

import classnames from 'classnames';

const Document = () => {
  return (
    <Html lang="ja">
      <Head />
      <body className={classnames('font-body', 'overflow-hidden')}>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
};

export default Document;
