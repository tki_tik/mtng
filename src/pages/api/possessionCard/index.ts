import { NextApiRequest, NextApiResponse } from 'next';

import { Db, MongoClient } from 'mongodb';

const client = new MongoClient(process.env.NEXT_PUBLIC_MONGO_DB_URL);

const collectionName = 'PossessionCard';

const possessionCard = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const db = await connectDB();
    const collection = db.collection(collectionName);

    switch (req.method) {
      case 'GET': {
        const data = await collection.find().toArray();
        res.status(200).json(data);
        break;
      }
      case 'POST': {
        const result = await collection.insertOne(req.body);
        res.status(201).json({ message: `added successfully. InsertedId: ${result.insertedId}` });
        break;
      }
      case 'PUT': {
        const filter = { ...req.body.filter, setNumber: Number(req.body.filter.setNumber) };
        const update = req.body.update;
        const result = await collection.updateOne(filter, update, { upsert: false });
        if (result.modifiedCount > 0) {
          res
            .status(201)
            .json({ message: `updated successfully. UpsertedId: ${result.upsertedId}` });
        } else {
          res.status(404).json({ message: 'update target is not found.' });
        }
        break;
      }
      case 'DELETE': {
        const target = { ...req.query, setNumber: Number(req.query.setNumber) };
        const { deletedCount } = await collection.deleteOne(target);
        if (deletedCount > 0) {
          res.status(200).json({ message: 'deleted successfully.' });
        } else {
          res.status(404).json({ message: 'delete target is not found.' });
        }
        break;
      }
    }
  } catch (e) {
    res.status(500).json({ statusCode: 500, message: e });
  } finally {
    closeDB();
  }
};

const connectDB = async (): Promise<Db> => {
  try {
    await client.connect();
    console.log('successfully connected to MongoDB.');
    return client.db('utmtng01');
  } catch (e) {
    console.log('unconnected to MongoDB.', e);
    throw new Error();
  }
};

const closeDB = async (): Promise<Db> => {
  try {
    await client.close();
    console.log('successfully closed to MongoDB.');
    return client.db('utmtng01');
  } catch (e) {
    console.log('unclosed to MongoDB.', e);
    throw new Error();
  }
};

export default possessionCard;
