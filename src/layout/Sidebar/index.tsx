import { FC, useState } from 'react';

import Presenter from './Presenter';

const Sidebar: FC = () => {
  const [isOpenSidebar, setIsOpenSidebar] = useState<boolean>(false);

  const onMouseOver = () => {
    setIsOpenSidebar(true);
  };

  const onMouseLeave = () => {
    setTimeout(() => {
      setIsOpenSidebar(false);
    }, 500);
  };

  return (
    <Presenter
      isOpenSidebar={isOpenSidebar}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
    />
  );
};

export default Sidebar;
