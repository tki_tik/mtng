import { atom } from 'recoil';

import AtomKeys from '../AtomKeys';
import { ToastInfo } from '@components/Toast';
import { MessageLevel } from '@types';

const initialState: ToastInfo = {
  level: MessageLevel.NONE,
  message: '',
};

const ShowToastStateAtom = atom<ToastInfo>({
  key: AtomKeys.SHOW_TOAST_STATE,
  default: initialState,
});

export default ShowToastStateAtom;
