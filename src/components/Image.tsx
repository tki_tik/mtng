import { FC, ImgHTMLAttributes } from 'react';

import classnames from 'classnames';

interface ImageProps extends ImgHTMLAttributes<HTMLImageElement> {
  type: string;
  objectContain: boolean;
}

const Image: FC<ImageProps> = ({ type, objectContain, src, alt }) => {
  return (
    <picture>
      <source srcSet={src ?? '/img/no-image.png'} type={type} />
      <img
        className={classnames('w-full', 'h-full', objectContain && 'object-contain')}
        src={src ?? '/img/no-image.png'}
        alt={alt}
      />
    </picture>
  );
};

export default Image;
