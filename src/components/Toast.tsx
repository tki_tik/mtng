import { FC, useEffect } from 'react';

import classnames from 'classnames';
import {
  MdCheckCircleOutline,
  MdErrorOutline,
  MdInfoOutline,
  MdWarningAmber,
} from 'react-icons/md';
import { useRecoilState } from 'recoil';

import ShowToastStateAtom from '@recoil/atoms/ShowToastStateAtom';
import { MessageLevel } from '@types';

export interface ToastInfo {
  level: MessageLevel;
  message: string;
}

const Toast: FC = () => {
  const [toastInfo, setToastInfo] = useRecoilState<ToastInfo>(ShowToastStateAtom);

  useEffect(() => {
    toastInfo.level !== MessageLevel.ERROR &&
      setTimeout(() => {
        setToastInfo({
          level: MessageLevel.NONE,
          message: '',
        });
      }, 5500);
  }, [setToastInfo, toastInfo.level]);

  const level2ClassNames = (): Array<string> => {
    switch (toastInfo.level) {
      case MessageLevel.INFO:
        return ['bg-blue-100', 'text-blue-900', 'animate-fadeinout'];
      case MessageLevel.SUCCESS:
        return ['bg-green-100', 'text-green-900', 'animate-fadeinout'];
      case MessageLevel.WARN:
        return ['bg-yellow-100', 'text-yellow-900', 'animate-fadeinout'];
      default:
        return ['bg-red-100', 'text-red-900', 'animate-fadein'];
    }
  };

  const level2Icon = (): JSX.Element => {
    switch (toastInfo.level) {
      case MessageLevel.INFO:
        return <MdInfoOutline className={classnames('text-blue-500')} size={20} />;
      case MessageLevel.SUCCESS:
        return <MdCheckCircleOutline className={classnames('text-green-500')} size={20} />;
      case MessageLevel.WARN:
        return <MdWarningAmber className={classnames('text-yellow-500')} size={20} />;
      default:
        return <MdErrorOutline className={classnames('text-red-500')} size={20} />;
    }
  };

  return (
    <div
      className={classnames(
        'absolute',
        'flex',
        'gap-2',
        'top-20',
        'right-8',
        'py-3',
        'px-4',
        'rounded',
        level2ClassNames(),
        'drop-shadow-md',
        'cursor-pointer',
        'z-[999]',
      )}
      onClick={() => setToastInfo({ ...toastInfo, level: MessageLevel.NONE })}
    >
      <div className={classnames('flex')}>{level2Icon()}</div>
      <p className={classnames('leading-5', 'text-xs')}>{toastInfo.message}</p>
    </div>
  );
};

export default Toast;
