import { Dispatch, FC, OptionHTMLAttributes, SetStateAction } from 'react';

import classnames from 'classnames';
import { MdSearch } from 'react-icons/md';

import Button from '@components/Button';
import Select from '@components/Select';
import Text from '@components/Text';

interface Props {
  set: string;
  setSet: Dispatch<SetStateAction<string>>;
  langOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>>;
  lang: string;
  setLang: Dispatch<SetStateAction<string>>;
  number: string;
  setNumber: Dispatch<SetStateAction<string>>;
  onSearchClick: () => void;
}

const Presenter: FC<Props> = ({
  set,
  setSet,
  langOptionList,
  lang,
  setLang,
  number,
  setNumber,
  onSearchClick,
}) => {
  return (
    <div
      className={classnames(
        'flex',
        'justify-center',
        'items-center',
        'gap-4',
        'w-full',
        'pb-4',
        'border-b',
        'border-gray-300',
      )}
    >
      <div className={classnames('w-24')}>
        <Text label={'Set'} setValue={setSet} value={set} type={'text'} />
      </div>
      <div className={classnames('h-full', 'bg-white')}>
        <Select optionList={langOptionList} setValue={setLang} value={lang} />
      </div>
      <div className={classnames('w-24')}>
        <Text label={'Number'} setValue={setNumber} value={number} type={'number'} />
      </div>
      <div className={classnames('w-12', 'h-full')}>
        <Button onClick={onSearchClick}>
          <MdSearch size={24} />
        </Button>
      </div>
    </div>
  );
};

export default Presenter;
