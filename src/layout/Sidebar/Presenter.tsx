import NextImage from 'next/image';
import { FC } from 'react';

import classnames from 'classnames';
import { GiCardDraw, GiHamburgerMenu } from 'react-icons/gi';
import { GiCardRandom } from 'react-icons/gi';
import { MdOutlinePostAdd } from 'react-icons/md';

import Menu from '@layout/Menu';
import logo from '@public/img/mtg-logo.png';

interface Props {
  isOpenSidebar: boolean;
  onMouseOver: () => void;
  onMouseLeave: () => void;
}

const Presenter: FC<Props> = ({ isOpenSidebar, onMouseOver, onMouseLeave }) => {
  return (
    <aside
      className={classnames(
        'fixed',
        'flex',
        'flex-col',
        'justify-between',
        'w-20',
        'h-screen',
        'bg-black',
        'bg-opacity-80',
        'transition-width',
        'duration-500',
        'hover:w-64',
        'z-10',
      )}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
    >
      <div>
        <div className={classnames('flex', 'justify-center', 'items-center', 'text-orange-600')}>
          <div className={classnames('flex', 'justify-center', 'items-center', 'h-24')}>
            {isOpenSidebar ? (
              <NextImage
                src={logo}
                className={classnames('w-full', 'px-7')}
                height={64}
                alt="logo"
              />
            ) : (
              <GiHamburgerMenu size={32} />
            )}
          </div>
        </div>
        <Menu Icon={GiCardRandom} label={'カード一覧'} path={'/'} isDispLabel={isOpenSidebar} />
        <Menu
          Icon={MdOutlinePostAdd}
          label={'カード追加'}
          path={'/add-card'}
          isDispLabel={isOpenSidebar}
        />
        <Menu
          Icon={GiCardDraw}
          label={'デッキ構築'}
          path={'/deck-build'}
          isDispLabel={isOpenSidebar}
        />
      </div>
    </aside>
  );
};

export default Presenter;
