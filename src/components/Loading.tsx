import { FC } from 'react';

import classnames from 'classnames';
import { SpinningCircles } from 'react-loading-icons';

const Loading: FC = () => {
  return (
    <div
      className={classnames(
        'absolute',
        'top-0',
        'flex',
        'justify-center',
        'items-center',
        'w-full',
        'h-full',
        'bg-black',
        'opacity-80',
        'z-[999]',
      )}
    >
      <SpinningCircles />
    </div>
  );
};

export default Loading;
