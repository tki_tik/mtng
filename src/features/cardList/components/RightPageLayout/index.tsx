import { FC, ReactNode } from 'react';

import Presenter from './Presenter';

interface RightPageLayoutProps {
  isOpen: boolean;
  coverColorClass?: string;
  zIndex: number;
  children: ReactNode;
}

const RightPageLayout: FC<RightPageLayoutProps> = ({
  isOpen,
  coverColorClass,
  zIndex,
  children,
}) => {
  return (
    <Presenter isOpen={isOpen} coverColorClass={coverColorClass} zIndex={zIndex}>
      {children}
    </Presenter>
  );
};

export default RightPageLayout;
