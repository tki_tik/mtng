import { Dispatch, FC, OptionHTMLAttributes, SetStateAction } from 'react';

import classnames from 'classnames';

import ModalBase from './ModalBase';
import Button from '@components/Button';
import Select from '@components/Select';
import { LANG_TYPE } from '@constants';

interface SetLangModalProps {
  lang: string;
  setLang: Dispatch<SetStateAction<string>>;
  onSettingClick: () => void;
  isOpen: boolean;
  onClose: () => void;
}

const SetLangModal: FC<SetLangModalProps> = ({
  lang,
  setLang,
  onSettingClick,
  isOpen,
  onClose,
}) => {
  const langOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>> = Object.keys(LANG_TYPE).map(
    (key) => {
      return { label: key, value: LANG_TYPE[key] };
    },
  );

  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div
        className={classnames(
          'flex',
          'flex-col',
          'items-center',
          'w-fit',
          'h-fit',
          'gap-4',
          'px-16',
          'py-6',
          'rounded',
          'bg-[url("/img/bg-image.jpg")]',
        )}
      >
        <div className={classnames('flex-grow', 'w-fit', 'pt-6')}>
          <div className={classnames('h-12', 'bg-white')}>
            <Select optionList={langOptionList} setValue={setLang} value={lang} />
          </div>
        </div>
        <div className={classnames('flex', 'justify-center', 'w-full', 'h-12')}>
          <div className={classnames('w-24', 'h-full')}>
            <Button onClick={onSettingClick}>
              <span>設定</span>
            </Button>
          </div>
        </div>
      </div>
    </ModalBase>
  );
};

export default SetLangModal;
