/** @type {import('tailwindcss').Config} */
import plugin from 'tailwindcss/plugin';

const backfaceVisibility = plugin(({ addUtilities }) => {
  addUtilities({
    '.backface-visible': {
      'backface-visibility': 'visible',
    },
    '.backface-hidden': {
      'backface-visibility': 'hidden',
    },
  });
});

module.exports = {
  content: [
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './features/**/*.{js,ts,jsx,tsx,mdx}',
    './layout/**/*.{js,ts,jsx,tsx,mdx}',
    './modal/**/*.{js,ts,jsx,tsx,mdx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        body: ["'M PLUS 1p'", 'sans-serif'],
      },
      dropShadow: {
        'lg-black': ['0 10px 8px rgb(0 0 0 / 0.1)', '0 4px 3px rgb(0 0 0 / 1)'],
      },
      keyframes: {
        fadein: {
          '0%': {
            opacity: '0',
            transform: 'translateX(400px)',
          },
          '100%': {
            opacity: '1',
            transform: 'translateX(0)',
          },
        },
        fadeout: {
          '0%': {
            opacity: '1',
            transform: 'translateX(0)',
          },
          '100%': {
            opacity: '0',
            transform: 'translateX(400px)',
          },
        },
      },
      animation: {
        fadein: 'fadein 0.5s linear forwards',
        fadeinout: 'fadein 0.5s linear forwards, fadeout 0.5s ease 5s forwards',
      },
    },
  },
  plugins: [backfaceVisibility],
};
