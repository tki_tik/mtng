import { atom } from 'recoil';

import AtomKeys from '../AtomKeys';

const LoadingStateAtom = atom<boolean>({
  key: AtomKeys.LOADING_STATE,
  default: false,
});

export default LoadingStateAtom;
