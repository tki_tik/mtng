import { Dispatch, FC, SetStateAction, useCallback, useEffect, useState } from 'react';

import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import { MdCheck, MdEdit } from 'react-icons/md';
import { useRecoilValue } from 'recoil';

import Button from '@components/Button';
import Image from '@components/Image';
import SetImageUrlModal from '@modal/SetImageUrlModal';
import SetLangModal from '@modal/SetLangModal';
import SymbologyStateAtom from '@recoil/atoms/SymbologyStateAtom';
import { PossessionCard, Symbology } from '@types';

interface CardInfoProps {
  isEdit?: boolean;
  setIsEdit?: Dispatch<SetStateAction<boolean>>;
  card: PossessionCard;
  setCard?: Dispatch<SetStateAction<PossessionCard>>;
}

const CardInfo: FC<CardInfoProps> = ({ isEdit, setIsEdit, card, setCard }) => {
  const symbology = useRecoilValue<Array<Symbology>>(SymbologyStateAtom);
  const [isDispSetLangModal, setIsDispSetLangModal] = useState<boolean>(false);
  const [isDispSetImageUrlModal, setIsDispSetImageUrlModal] = useState<boolean>(false);
  const [editLang, setEditLang] = useState<string>('');
  const [editName, setEditName] = useState<string>('');
  const [editImageUrl, setEditImageUrl] = useState<string>('');
  const [editType, setEditType] = useState<string>('');
  const [editText, setEditText] = useState<string>('');
  const [editFlavor, setEditFlavor] = useState<string>('');

  const { lang, name, originalName, imageUrl, manaCost, type, text, flavor } = card;

  const text2SymbologyIcon = (text: string, symbology: Array<Symbology>): string => {
    let result = text;
    symbology.forEach(({ symbol, svg_uri, english }) => {
      result = result.replaceAll(
        symbol,
        `<img style="display: inline; width: auto; height: 16px; margin: 0 0.25em; vertical-align: middle;" src="${svg_uri}" alt=${english}>`,
      );
    });
    return result;
  };

  const setEditData = useCallback(() => {
    setEditLang(lang);
    setEditName(name ?? originalName);
    setEditImageUrl(imageUrl ?? '');
    setEditType(type);
    setEditText(text);
    flavor && setEditFlavor(flavor);
  }, [flavor, imageUrl, lang, name, originalName, text, type]);

  const onCheckClick = () => {
    setCard &&
      setCard({
        ...card,
        name: editName ?? name,
        type: editType ?? type,
        text: editText ?? text,
        flavor: editFlavor ?? flavor,
        imageUrl: editImageUrl,
      });
    setIsEdit && setIsEdit(false);
  };

  const onSettingLangClick = () => {
    setCard &&
      setCard({
        ...card,
        lang: editLang,
      });
    setIsDispSetLangModal(false);
    setIsEdit && setIsEdit(true);
  };

  const onSettingImageUrlClick = () => {
    setCard &&
      setCard({
        ...card,
        name: editName ?? name,
        type: editType ?? type,
        text: editText ?? text,
        flavor: editFlavor ?? flavor,
        imageUrl: editImageUrl,
      });
    setIsDispSetImageUrlModal(false);
  };

  const onImageClick = () => {
    isEdit && setIsDispSetImageUrlModal(true);
  };

  useEffect(() => {
    setEditData();
  }, [setEditData]);

  return (
    <div className={classnames('relative', 'flex', 'flex-col', 'gap-8', 'h-full')}>
      <div
        className={classnames(
          'h-8',
          'pl-4',
          'border-l-4',
          'border-orange-600',
          'font-bold',
          setIsEdit && 'mr-16',
        )}
      >
        {isEdit ? (
          <>
            <input
              className={classnames(
                'w-full',
                'h-full',
                'font-bold',
                'text-2xl',
                'bg-transparent',
                'outline-none',
              )}
              type={'text'}
              value={editName}
              onChange={(e) => setEditName(e.target.value)}
            />
            <div className={classnames('absolute', 'top-0', 'right-0', 'w-12', 'h-12')}>
              <Button onClick={onCheckClick}>
                <MdCheck size={24} />
              </Button>
            </div>
          </>
        ) : (
          <>
            <h2 className={classnames('font-bold', 'text-2xl', 'truncate')}>
              {name === originalName ? name : `${name} / ${originalName}`}
            </h2>
            {setIsEdit && (
              <div className={classnames('absolute', 'top-0', 'right-0', 'w-12', 'h-12')}>
                <Button onClick={() => setIsDispSetLangModal(true)}>
                  <MdEdit size={24} />
                </Button>
              </div>
            )}
          </>
        )}
      </div>
      <div className={classnames('flex', 'items-center', 'gap-8', 'flex-grow', 'h-0')}>
        <div
          className={classnames(
            'w-1/3',
            'h-full',
            'drop-shadow-lg-black',
            isEdit && 'cursor-pointer',
          )}
          onClick={onImageClick}
        >
          <Image type={'image/png'} src={imageUrl} alt={name} objectContain={true} />
        </div>
        <div className={classnames('flex', 'flex-col', 'gap-5', 'flex-grow', 'w-2/3', 'h-full')}>
          <div className={classnames('flex', 'gap-2', 'h-9')}>
            {manaCost
              .split('}')
              .filter((x) => x)
              .map((x) => `${x}}`)
              .map((mana, i) => (
                <div className={classnames('w-9', 'h-9', 'drop-shadow-lg-black')} key={i}>
                  <Image
                    type={'images/svg'}
                    src={symbology.find(({ symbol }) => symbol === mana)?.svg_uri}
                    alt={symbology.find(({ symbol }) => symbol === mana)?.english}
                    objectContain={false}
                  />
                </div>
              ))}
          </div>
          <div className={classnames('h-8', 'pb-2', 'border-b', 'border-gray-300', 'font-bold')}>
            {isEdit ? (
              <input
                className={classnames(
                  'w-full',
                  'h-full',
                  'font-bold',
                  'bg-transparent',
                  'outline-none',
                )}
                type={'text'}
                value={editType}
                onChange={(e) => setEditType(e.target.value)}
              />
            ) : (
              <p className={classnames('h-full', 'font-bold', 'truncate')}>{type}</p>
            )}
          </div>
          <div className={classnames('flex-grow')}>
            <div className={classnames('flex', 'items-center', flavor ? 'h-3/4' : 'h-full')}>
              {isEdit ? (
                <textarea
                  className={classnames(
                    'w-full',
                    'h-full',
                    'text-gray-700',
                    'text-sm',
                    'bg-transparent',
                    'resize-none',
                    'outline-none',
                  )}
                  value={editText}
                  onChange={(e) => setEditText(e.target.value)}
                />
              ) : (
                <p
                  className={classnames(
                    'h-fit',
                    'text-gray-700',
                    'text-sm',
                    'leading-loose',
                    'tracking-wide',
                    'whitespace-pre-wrap',
                  )}
                >
                  {ReactHtmlParser(text2SymbologyIcon(text, symbology))}
                </p>
              )}
            </div>
            {flavor && (
              <div
                className={classnames(
                  'flex',
                  'items-center',
                  'h-1/4',
                  'border-t',
                  'border-gray-300',
                )}
              >
                {isEdit ? (
                  <textarea
                    className={classnames(
                      'w-full',
                      'h-full',
                      'pt-5',
                      'text-gray-700',
                      'text-sm',
                      'bg-transparent',
                      'resize-none',
                      'outline-none',
                    )}
                    value={editFlavor}
                    onChange={(e) => setEditFlavor(e.target.value)}
                  />
                ) : (
                  <p className={classnames('text-gray-700', 'text-sm')}>{flavor}</p>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
      <SetLangModal
        lang={editLang}
        setLang={setEditLang}
        onSettingClick={onSettingLangClick}
        isOpen={isDispSetLangModal}
        onClose={() => setIsDispSetLangModal(false)}
      />
      <SetImageUrlModal
        imageUrl={editImageUrl}
        setImageUrl={setEditImageUrl}
        onSettingClick={onSettingImageUrlClick}
        isOpen={isDispSetImageUrlModal}
        onClose={() => setIsDispSetImageUrlModal(false)}
      />
    </div>
  );
};

export default CardInfo;
