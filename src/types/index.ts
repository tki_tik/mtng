import { Legalities } from '@features/addCard/types';

export enum MessageLevel {
  NONE,
  INFO,
  SUCCESS,
  WARN,
  ERROR,
}

export enum ManaColor {
  W = 'W',
  U = 'U',
  B = 'B',
  R = 'R',
  G = 'G',
  C = 'C',
}

export interface Symbology {
  symbol: string;
  svg_uri: string;
  loose_variant?: string;
  english: string;
  transposable: boolean;
  represents_mana: boolean;
  appears_in_mana_costs: boolean;
  mana_value?: number;
  cmc?: number;
  funny: boolean;
  colors: Array<ManaColor>;
  gatherer_alternates?: Array<string>;
}

export interface PossessionCard {
  _id?: string;
  multiverseid: number;
  lang: string;
  name: string;
  originalName: string;
  imageUrl?: string;
  set: string;
  setNumber: number;
  colorIdentity: Array<string>;
  manaCost: string;
  cmc: number;
  rarity: string;
  type: string;
  originalType: string;
  text: string;
  flavor?: string;
  legalities: Legalities;
  foilType: string;
  number: string;
}
