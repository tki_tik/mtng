import { atom } from 'recoil';

import AtomKeys from '../AtomKeys';
import { Symbology } from '@types';

const SymbologyStateAtom = atom<Array<Symbology>>({
  key: AtomKeys.SYMBOLOGY_STATE,
  default: [],
});

export default SymbologyStateAtom;
