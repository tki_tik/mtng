import { GetStaticProps, NextPage } from 'next';

import classnames from 'classnames';

const DeckBuild: NextPage = () => {
  return (
    <div
      className={classnames(
        'flex',
        'justify-center',
        'items-center',
        'h-full',
        'text-white',
        'font-bold',
      )}
    >
      現在準備中
    </div>
  );
};

export const getStaticProps: GetStaticProps = () => {
  return {
    props: {
      title: 'デッキ構築',
    },
  };
};

export default DeckBuild;
