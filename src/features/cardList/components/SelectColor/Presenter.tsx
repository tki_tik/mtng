import { FC } from 'react';

import classnames from 'classnames';

import ColorIcon from '@features/cardList/components/ColorIcon';
import black from '@public/img/mtg-black.png';
import blue from '@public/img/mtg-blue.png';
import colorless from '@public/img/mtg-colorless.png';
import green from '@public/img/mtg-green.png';
import multicolor from '@public/img/mtg-multicolor.png';
import red from '@public/img/mtg-red.png';
import white from '@public/img/mtg-white.png';
import { ManaColor } from '@types';

const Presenter: FC = () => {
  return (
    <div className={classnames('flex', 'justify-center', 'gap-3', 'z-0')}>
      <ColorIcon color={ManaColor.W} colorImage={white} />
      <ColorIcon color={ManaColor.U} colorImage={blue} />
      <ColorIcon color={ManaColor.B} colorImage={black} />
      <ColorIcon color={ManaColor.R} colorImage={red} />
      <ColorIcon color={ManaColor.G} colorImage={green} />
      <ColorIcon color={ManaColor.C} colorImage={colorless} />
      <ColorIcon color={undefined} colorImage={multicolor} />
    </div>
  );
};

export default Presenter;
