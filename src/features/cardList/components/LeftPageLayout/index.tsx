import { FC, ReactNode } from 'react';

import Presenter from './Presenter';

interface LeftPageLayoutProps {
  isOpen: boolean;
  coverColorClass?: string;
  children: ReactNode;
}

const LeftPageLayout: FC<LeftPageLayoutProps> = ({ isOpen, coverColorClass, children }) => {
  return (
    <Presenter isOpen={isOpen} coverColorClass={coverColorClass}>
      {children}
    </Presenter>
  );
};

export default LeftPageLayout;
