import { ManaColor } from '@types';

export interface ImageUris {
  small: string;
  normal: string;
  large: string;
  png: string;
  art_crop: string;
  border_crop: string;
}

export interface CardFace {
  object: string;
  name: string;
  mana_cost: string;
  type_line: string;
  oracle_text: string;
  colors: Array<ManaColor>;
  power: string;
  toughness: string;
  flavor_text?: string;
  watermark: string;
  artist: string;
  artist_id: string;
  illustration_id: string;
  image_uris: ImageUris;
  flavor_name?: string;
}

export enum Legality {
  legal = 'legal',
  not_legal = 'not_legal',
  restricted = 'restricted',
  banned = 'banned',
}

export interface Legalities {
  standard: Legality;
  future: Legality;
  historic: Legality;
  gladiator: Legality;
  pioneer: Legality;
  explorer: Legality;
  modern: Legality;
  legacy: Legality;
  pauper: Legality;
  vintage: Legality;
  penny: Legality;
  commander: Legality;
  oathbreaker: Legality;
  brawl: Legality;
  historicbrawl: Legality;
  alchemy: Legality;
  paupercommander: Legality;
  duel: Legality;
  oldschool: Legality;
  premodern: Legality;
  predh: Legality;
}

export enum Rarity {
  common = 'common',
  uncommon = 'uncommon',
  rare = 'rare',
  special = 'special',
  mythic = 'mythic',
  bonus = 'bonus',
}

export enum BorderColor {
  black = 'black',
  white = 'white',
  borderless = 'borderless',
  silver = 'silver',
  gold = 'gold',
}

export interface Preview {
  source: string;
  source_uri: string;
  previewed_at: Date;
}

export interface Prices {
  usd: string;
  usd_foil: string;
  usd_etched: string;
  eur: string;
  eur_foil: string;
  tix: string;
}

export interface PurchaseUris {
  tcgplayer: string;
  cardmarket: string;
  cardhoarder: string;
}

export interface RelatedUris {
  gatherer: string;
  tcgplayer_infinite_articles: string;
  tcgplayer_infinite_decks: string;
  edhrec: string;
}

export interface Card {
  object: string;
  id: string;
  oracle_id: string;
  multiverse_ids: Array<number>;
  mtgo_id: number;
  arena_id: number;
  tcgplayer_id: number;
  cardmarket_id: number;
  name: string;
  printed_name: string;
  lang: string;
  released_at: Date;
  uri: string;
  scryfall_uri: string;
  layout: string;
  highres_image: boolean;
  image_status: string;
  image_uris: ImageUris;
  mana_cost: string;
  cmc: number;
  type_line: string;
  printed_type_line: string;
  oracle_text: string;
  printed_text: string;
  loyalty: string;
  colors: Array<ManaColor>;
  color_identity: Array<ManaColor>;
  keywords: Array<string>;
  card_faces: Array<CardFace>;
  legalities: Legalities;
  games: Array<string>;
  reserved: boolean;
  foil: boolean;
  nonfoil: boolean;
  finishes: Array<string>;
  oversized: boolean;
  promo: boolean;
  reprint: boolean;
  variation: boolean;
  set_id: string;
  set: string;
  set_name: string;
  set_type: string;
  set_uri: string;
  set_search_uri: string;
  scryfall_set_uri: string;
  rulings_uri: string;
  prints_search_uri: string;
  collector_number: string;
  digital: boolean;
  rarity: Rarity;
  flavor_text?: string;
  card_back_id: string;
  artist: string;
  artist_ids: Array<string>;
  illustration_id: string;
  border_color: BorderColor;
  frame: string;
  frame_effects: Array<string>;
  security_stamp: string;
  full_art: boolean;
  textless: boolean;
  booster: boolean;
  story_spotlight: boolean;
  edhrec_rank: number;
  penny_rank: number;
  preview: Preview;
  prices: Prices;
  related_uris: RelatedUris;
  purchase_uris: PurchaseUris;
}
