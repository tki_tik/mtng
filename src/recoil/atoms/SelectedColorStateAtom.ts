import { atom } from 'recoil';

import AtomKeys from '../AtomKeys';
import { ManaColor } from '@types';

const SelectedColorStateAtom = atom<ManaColor | undefined>({
  key: AtomKeys.SELECTED_COLOR_STATE,
  default: ManaColor.W,
});

export default SelectedColorStateAtom;
