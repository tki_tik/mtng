import { StaticImageData } from 'next/image';
import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';

import { useRecoilValue } from 'recoil';

import Presenter from './Presenter';
import SelectedColorStateAtom from '@recoil/atoms/SelectedColorStateAtom';
import { ManaColor } from '@types';

interface CoverFrontProps {
  zIndex: number;
  colorLogo: StaticImageData;
  bgColorClass: string;
  borderColorClass: string;
  setIsCardFileOpen: Dispatch<SetStateAction<boolean>>;
}

const CoverFront: FC<CoverFrontProps> = ({
  zIndex,
  colorLogo,
  bgColorClass,
  borderColorClass,
  setIsCardFileOpen,
}) => {
  const selectedColor = useRecoilValue<ManaColor | undefined>(SelectedColorStateAtom);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    setIsOpen(false);
  }, [selectedColor]);

  useEffect(() => {
    setIsCardFileOpen(isOpen);
  }, [isOpen, setIsCardFileOpen]);

  return (
    <Presenter
      isOpen={isOpen}
      setIsOpen={setIsOpen}
      zIndex={zIndex}
      bgColorClass={bgColorClass}
      borderColorClass={borderColorClass}
      colorLogo={colorLogo}
    />
  );
};

export default CoverFront;
