import { FC, ReactNode, useEffect } from 'react';

import { useRecoilState, useRecoilValue } from 'recoil';

import Presenter from './Presenter';
import { ToastInfo } from '@components/Toast';
import LoadingStateAtom from '@recoil/atoms/LoadingStateAtom';
import ShowToastStateAtom from '@recoil/atoms/ShowToastStateAtom';
import { MessageLevel } from '@types';

export interface LayoutProps {
  title: string;
  children: ReactNode;
}

const Layout: FC<LayoutProps> = ({ title, children }) => {
  const loading = useRecoilValue<boolean>(LoadingStateAtom);
  const [toastInfo, setToastInfo] = useRecoilState<ToastInfo>(ShowToastStateAtom);

  useEffect(() => {
    loading && setToastInfo({ level: MessageLevel.NONE, message: '' });
  }, [loading, setToastInfo]);

  return (
    <Presenter loading={loading} toastInfo={toastInfo} title={title}>
      {children}
    </Presenter>
  );
};

export default Layout;
