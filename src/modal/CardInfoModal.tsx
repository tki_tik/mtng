import { FC, OptionHTMLAttributes } from 'react';

import classnames from 'classnames';
import { useSetRecoilState } from 'recoil';

import ModalBase from './ModalBase';
import Button from '@components/Button';
import CardInfo from '@components/CardInfo';
import Select from '@components/Select';
import Text from '@components/Text';
import { ToastInfo } from '@components/Toast';
import { FOIL_TYPE } from '@constants';
import AllCardStateAtom, { AllCardState } from '@recoil/atoms/AllCardStateAtom';
import LoadingStateAtom from '@recoil/atoms/LoadingStateAtom';
import ShowToastStateAtom from '@recoil/atoms/ShowToastStateAtom';
import { MessageLevel, PossessionCard } from '@types';

interface CardInfoModalProps {
  card: PossessionCard;
  isOpen: boolean;
  onClose: () => void;
}

const CardInfoModal: FC<CardInfoModalProps> = ({ card, isOpen, onClose }) => {
  const setAllCards = useSetRecoilState<AllCardState>(AllCardStateAtom);
  const setLoading = useSetRecoilState<boolean>(LoadingStateAtom);
  const setToastInfo = useSetRecoilState<ToastInfo>(ShowToastStateAtom);

  const foilOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>> = Object.values(FOIL_TYPE)
    .map((value, i) => {
      return { label: value, value: i };
    })
    .sort((a, b) => b.value - a.value);

  const onDeleteClick = async () => {
    setLoading(true);
    const res = await remove();
    if (res.status === 200) {
      setToastInfo({
        level: MessageLevel.SUCCESS,
        message: '正常に削除されました',
      });
      const res = await fetch('/api/possessionCard');
      const data = await res.json();
      setAllCards({ acquired: true, data });
      onClose();
    } else if (res.status === 404) {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: '削除対象が見つかりません',
      });
    } else {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: '予期し無いエラーが発生しました',
      });
    }
    setLoading(false);
  };

  const remove = async () => {
    const res = await fetch(
      `/api/possessionCard?set=${card.set}&setNumber=${card.setNumber}&lang=${card.lang}&foilType=${card.foilType}`,
      {
        method: 'DELETE',
      },
    );
    return res;
  };

  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div
        className={classnames(
          'flex',
          'flex-col',
          'w-[66vw]',
          'h-[75vh]',
          'min-h-[624px]',
          'gap-4',
          'px-16',
          'py-6',
          'rounded',
          'bg-[url("/img/bg-image.jpg")]',
        )}
      >
        <div className={classnames('flex-grow', 'w-full', 'py-6', 'border-b', 'border-gray-300')}>
          <CardInfo card={card} />
        </div>
        <div className={classnames('flex', 'justify-end', 'gap-4', 'w-full', 'h-12')}>
          <div className={classnames('h-full', 'bg-white')}>
            <Select optionList={foilOptionList} value={card.foilType} disabled />
          </div>
          <div className={classnames('w-20')}>
            <Text label={'所持枚数'} value={card.number} type={'number'} readOnly />
          </div>
          <div className={classnames('w-24', 'h-full')}>
            <Button onClick={onDeleteClick}>
              <span>削除</span>
            </Button>
          </div>
        </div>
      </div>
    </ModalBase>
  );
};

export default CardInfoModal;
