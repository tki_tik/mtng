import { FC, useEffect, useState } from 'react';

import { useRecoilValue } from 'recoil';

import Presenter from './Presenter';
import SelectedColorStateAtom from '@recoil/atoms/SelectedColorStateAtom';
import { ManaColor, PossessionCard } from '@types';

interface FilePageProps {
  zIndex: number;
  cardList: Array<PossessionCard>;
}

const FilePage: FC<FilePageProps> = ({ zIndex, cardList }) => {
  const selectedColor = useRecoilValue<ManaColor | undefined>(SelectedColorStateAtom);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    setIsOpen(false);
  }, [selectedColor]);

  return <Presenter isOpen={isOpen} setIsOpen={setIsOpen} zIndex={zIndex} cardList={cardList} />;
};

export default FilePage;
