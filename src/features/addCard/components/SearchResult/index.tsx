import {
  Dispatch,
  FC,
  OptionHTMLAttributes,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';

import { useRecoilState, useSetRecoilState } from 'recoil';

import Presenter from './Presenter';
import { ToastInfo } from '@components/Toast';
import { FOIL_TYPE, INITIAL_POSSESSION_CARD } from '@constants';
import { Card } from '@features/addCard/types';
import AllCardStateAtom, { AllCardState } from '@recoil/atoms/AllCardStateAtom';
import LoadingStateAtom from '@recoil/atoms/LoadingStateAtom';
import ShowToastStateAtom from '@recoil/atoms/ShowToastStateAtom';
import { ManaColor, MessageLevel, PossessionCard } from '@types';

interface SearchResultProps {
  card: Card;
  setCard: Dispatch<SetStateAction<Card | undefined>>;
}

const SearchResult: FC<SearchResultProps> = ({ card, setCard }) => {
  const setLoading = useSetRecoilState<boolean>(LoadingStateAtom);
  const setToastInfo = useSetRecoilState<ToastInfo>(ShowToastStateAtom);
  const [allCards, setAllCards] = useRecoilState<AllCardState>(AllCardStateAtom);
  const [isCardInfoEdit, setIsCardInfoEdit] = useState<boolean>(false);
  const [possession, setPossession] = useState<string>('0');
  const [possessionCard, setPossessionCard] = useState<PossessionCard>(INITIAL_POSSESSION_CARD);
  const [foilType, setFoilType] = useState<string>('0');
  const [number, setNumber] = useState<string>('1');
  const [isDisabledRegistration, setIsDisabledRegistration] = useState<boolean>(false);

  const foilOptionList: Array<OptionHTMLAttributes<HTMLOptionElement>> = Object.values(FOIL_TYPE)
    .map((value, i) => {
      return { label: value, value: i };
    })
    .sort((a, b) => b.value - a.value);

  const card2PossessionCard = useCallback((from: Card): PossessionCard => {
    const {
      multiverse_ids,
      lang,
      printed_name,
      name,
      image_uris,
      set,
      collector_number,
      color_identity,
      mana_cost,
      card_faces,
      cmc,
      rarity,
      printed_type_line,
      type_line,
      printed_text,
      oracle_text,
      flavor_text,
      legalities,
    } = from;
    return {
      multiverseid: multiverse_ids[0],
      lang,
      name: printed_name ?? name,
      originalName: name,
      imageUrl: image_uris
        ? image_uris.png
        : multiverse_ids && multiverse_ids[0]
        ? `https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=${multiverse_ids[0]}&type=card`
        : undefined,
      set,
      setNumber: Number(collector_number),
      colorIdentity: color_identity.length > 0 ? color_identity : [ManaColor.C],
      manaCost: mana_cost ?? card_faces[0].mana_cost,
      cmc,
      rarity,
      type: card_faces ? card_faces[0].type_line : printed_type_line ?? type_line,
      originalType: card_faces ? card_faces[0].type_line : type_line,
      text: printed_text ?? oracle_text ?? card_faces[0].oracle_text,
      flavor: card_faces ? card_faces[0].flavor_text : flavor_text,
      legalities,
      foilType: '0',
      number: '1',
    };
  }, []);

  const onRegistrationClick = async () => {
    setLoading(true);
    const { status } = Number(possession) > 0 ? await update() : await registration();
    if (status === 201) {
      setToastInfo({
        level: MessageLevel.SUCCESS,
        message: `${card.printed_name ?? card.name} を登録しました`,
      });
      setCard(undefined);
      const res = await fetch('/api/possessionCard');
      const data = await res.json();
      setAllCards({ acquired: true, data });
    } else if (status === 404) {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: '更新対象が存在しません',
      });
    } else {
      setToastInfo({
        level: MessageLevel.ERROR,
        message: '予期し無いエラーが発生しました',
      });
    }
    setLoading(false);
  };

  const registration = async (): Promise<Response> => {
    const reqBody = possessionCard;
    const res = await fetch('/api/possessionCard', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(reqBody),
    });
    return res;
  };

  const update = async (): Promise<Response> => {
    const reqBody = {
      filter: {
        set: card.set,
        setNumber: card.collector_number,
        lang: card.lang,
        foilType: foilType,
      },
      update: { $set: { number: Number(possession) + Number(number) } },
    };
    const res = await fetch('/api/possessionCard', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(reqBody),
    });
    return res;
  };

  useEffect(() => {
    setIsDisabledRegistration(isCardInfoEdit);
  }, [isCardInfoEdit]);

  useEffect(() => {
    setIsDisabledRegistration(Number(number) < 1);
  }, [number]);

  useEffect(() => {
    if (card) {
      setPossessionCard(card2PossessionCard(card));
      setNumber('1');
      setFoilType('0');
    }
  }, [card, card2PossessionCard]);

  useEffect(() => {
    setPossessionCard((prev) => {
      return { ...prev, foilType, number };
    });
  }, [foilType, number]);

  useEffect(() => {
    const possessed = allCards.data.find(
      ({ set, setNumber, lang, foilType: foil }) =>
        set === possessionCard.set &&
        setNumber === possessionCard.setNumber &&
        lang === possessionCard.lang &&
        foil === foilType,
    );
    setPossession(possessed?.number ?? '0');
  }, [allCards, foilType, possessionCard.lang, possessionCard.set, possessionCard.setNumber]);

  return (
    <Presenter
      isCardInfoEdit={isCardInfoEdit}
      setIsCardInfoEdit={setIsCardInfoEdit}
      possession={possession}
      card={possessionCard}
      setCard={setPossessionCard}
      foilOptionList={foilOptionList}
      foilType={foilType}
      setFoilType={setFoilType}
      number={number}
      setNumber={setNumber}
      onRegistrationClick={onRegistrationClick}
      isDisabledRegistration={isDisabledRegistration}
    />
  );
};

export default SearchResult;
