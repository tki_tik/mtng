import NextImage, { StaticImageData } from 'next/image';
import { Dispatch, FC, SetStateAction } from 'react';

import classnames from 'classnames';

import { ManaColor } from '@types';

interface Props {
  isSelected: boolean;
  setSelectedColor: Dispatch<SetStateAction<ManaColor | undefined>>;
  color?: ManaColor;
  isHover: boolean;
  setIsHover: Dispatch<SetStateAction<boolean>>;
  colorImage: StaticImageData;
  bgColorClass: string;
  afterBgColorClass: string;
}

const Presenter: FC<Props> = ({
  isSelected,
  setSelectedColor,
  color,
  isHover,
  setIsHover,
  colorImage,
  bgColorClass,
  afterBgColorClass,
}) => {
  return (
    <div
      className={classnames(
        'relative',
        'w-16',
        'h-16',
        'p-2',
        'border-2',
        'rounded-full',
        'text-center',
        'cursor-pointer',
        'z-[1]',
        'overflow-hidden',
        'after:absolute',
        'after:inset-0',
        'after:w-full',
        'after:h-full',
        'after:content-[""]',
        'after:origin-top-left',
        'after:ease-in-out',
        'after:duration-300',
        'after:z-[-1]',
        afterBgColorClass,
        isSelected ? [bgColorClass, 'border-orange-600'] : ['bg-transparent', 'border-transparent'],
        !isSelected && (isHover ? ['after:scale-100'] : ['after:scale-x-0', 'after:scale-y-100']),
      )}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onClick={() => setSelectedColor(color)}
    >
      <NextImage className={classnames('w-full', 'h-full')} src={colorImage} alt="type_image" />
    </div>
  );
};

export default Presenter;
