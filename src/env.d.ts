declare module 'process' {
  global {
    namespace NodeJS {
      interface ProcessEnv {
        NEXT_PUBLIC_SCRYFALL_DOMAIN: string;
        NEXT_PUBLIC_MONGO_DB_URL: string;
      }
    }
  }
}
