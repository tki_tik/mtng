import { useRouter } from 'next/router';
import { FC } from 'react';

import { IconType } from 'react-icons/lib';

import Presenter from './Presenter';

export interface MenuProps {
  path: string;
  label: string;
  Icon: IconType;
  isDispLabel?: boolean;
}

const Menu: FC<MenuProps> = (props) => {
  const router = useRouter();

  return <Presenter {...props} currentPath={router.pathname} />;
};

export default Menu;
