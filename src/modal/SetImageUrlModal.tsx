import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';

import classnames from 'classnames';

import ModalBase from './ModalBase';
import Button from '@components/Button';
import Text from '@components/Text';

interface SetImageUrlModalProps {
  imageUrl: string;
  setImageUrl: Dispatch<SetStateAction<string>>;
  onSettingClick: () => void;
  isOpen: boolean;
  onClose: () => void;
}

const SetImageUrlModal: FC<SetImageUrlModalProps> = ({
  imageUrl,
  setImageUrl,
  onSettingClick,
  isOpen,
  onClose,
}) => {
  const [isSettingDisabled, setIsSettingDisabled] = useState<boolean>();

  useEffect(() => {
    setIsSettingDisabled(!imageUrl);
  }, [imageUrl]);

  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div
        className={classnames(
          'flex',
          'flex-col',
          'items-center',
          'w-[66vw]',
          'h-fit',
          'gap-4',
          'px-16',
          'py-6',
          'rounded',
          'bg-[url("/img/bg-image.jpg")]',
        )}
      >
        <div className={classnames('flex-grow', 'w-full', 'pt-6')}>
          <Text
            className={classnames('w-full', 'h-full')}
            type={'url'}
            label={'画像URL'}
            setValue={setImageUrl}
            value={imageUrl}
          />
        </div>
        <div className={classnames('flex', 'justify-center', 'w-full', 'h-12')}>
          <div className={classnames('w-24', 'h-full')}>
            <Button onClick={onSettingClick} disabled={isSettingDisabled}>
              <span>設定</span>
            </Button>
          </div>
        </div>
      </div>
    </ModalBase>
  );
};

export default SetImageUrlModal;
