import { selector } from 'recoil';

import SelectorKeys from '../SelectorKeys';
import AllCardStateAtom from '../atoms/AllCardStateAtom';
import SelectedColorStateAtom from '../atoms/SelectedColorStateAtom';
import { CardType, INITIAL_POSSESSION_CARD } from '@constants';
import { Rarity } from '@features/addCard/types';
import { ManaColor, PossessionCard } from '@types';
import { groupBy } from '@util';

const PagingCardSelector = selector<Array<Array<PossessionCard>>>({
  key: SelectorKeys.PAGING_CARD_SELECTOR,
  get: ({ get }) => {
    const selectedColor = get(SelectedColorStateAtom);
    const { data } = get(AllCardStateAtom);
    const selectedColorCard = getSelectedColorCard(data, selectedColor);
    const sortedCardList = sortCard(selectedColorCard);
    return getPagingCardList(sortedCardList);
  },
});

const getSelectedColorCard = (
  cardList: Array<PossessionCard>,
  color?: ManaColor,
): Array<PossessionCard> => {
  const selectedColorCard = cardList.filter(({ colorIdentity }) => {
    if (color) {
      return colorIdentity.length === 1 && colorIdentity[0] === color;
    } else {
      return colorIdentity.length > 1;
    }
  });
  return selectedColorCard.length > 0 ? selectedColorCard : [INITIAL_POSSESSION_CARD];
};

const sortCard = (cardList: Array<PossessionCard>): Array<PossessionCard> => {
  return [...cardList].sort((a, b) => {
    if (getMatchingCardTypeKey(a.originalType) !== getMatchingCardTypeKey(b.originalType)) {
      return (
        Object.keys(CardType).indexOf(getMatchingCardTypeKey(a.originalType)) -
        Object.keys(CardType).indexOf(getMatchingCardTypeKey(b.originalType))
      );
    } else if (a.rarity !== b.rarity) {
      return Object.keys(Rarity).indexOf(b.rarity) - Object.keys(Rarity).indexOf(a.rarity);
    } else if (a.set !== b.set) {
      return a.set > b.set ? 0 : -1;
    } else if (a.setNumber !== b.setNumber) {
      return Number(a.setNumber) - Number(b.setNumber);
    } else {
      return Number(b.foilType) - Number(a.foilType);
    }
  });
};

const getPagingCardList = (cardList: Array<PossessionCard>): Array<Array<PossessionCard>> => {
  const groupedCards = groupBy(cardList, ({ originalType }) =>
    getMatchingCardTypeKey(originalType),
  );
  const halfPagingCardList = getHalfPagingCardList(groupedCards);
  const tempPagingCardList: Array<Array<PossessionCard>> = [];
  halfPagingCardList.forEach((_, i) => {
    i % 2 === 0 &&
      tempPagingCardList.push([
        ...halfPagingCardList[i],
        ...(halfPagingCardList[i + 1] ?? [...Array(9)].map(() => INITIAL_POSSESSION_CARD)),
      ]);
  });
  return tempPagingCardList;
};

const getMatchingCardTypeKey = (target: string): string => {
  return Object.keys(CardType).find((key: string) => target.includes(key)) ?? '';
};

const getHalfPagingCardList = (
  cardList: Record<string, Array<PossessionCard>>,
): Array<Array<PossessionCard>> => {
  const halfPagingCardList: Array<Array<PossessionCard>> = [];
  Object.values(cardList).forEach((value) => {
    for (let i = 0; i < value.length; i += 9) {
      const tempCardList = [...Array(9)].map((_, j) => {
        return value[i + j] ?? INITIAL_POSSESSION_CARD;
      });
      halfPagingCardList.push(tempCardList);
      if (value.length == i + 9) {
        halfPagingCardList.push([...Array(9)].map(() => INITIAL_POSSESSION_CARD));
      }
    }
  });
  return halfPagingCardList;
};

export default PagingCardSelector;
