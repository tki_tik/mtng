import { StaticImageData } from 'next/image';
import { FC, useCallback, useEffect, useState } from 'react';

import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

import Presenter from './Presenter';
import blackLogo from '@public/img/mtg-black.png';
import blueLogo from '@public/img/mtg-blue.png';
import colorlessLogo from '@public/img/mtg-colorless.png';
import greenLogo from '@public/img/mtg-green.png';
import multicolorLogo from '@public/img/mtg-multicolor.png';
import redLogo from '@public/img/mtg-red.png';
import whiteLogo from '@public/img/mtg-white.png';
import AllCardStateAtom, { AllCardState } from '@recoil/atoms/AllCardStateAtom';
import LoadingStateAtom from '@recoil/atoms/LoadingStateAtom';
import SelectedColorStateAtom from '@recoil/atoms/SelectedColorStateAtom';
import PagingCardSelector from '@recoil/selectors/PagingCardSelector';
import { ManaColor, PossessionCard } from '@types';

interface CardFileStyle {
  colorLogo: StaticImageData;
  bgColorClass: string;
  borderColorClass: string;
}

const CardFile: FC = () => {
  const setLoading = useSetRecoilState<boolean>(LoadingStateAtom);
  const [allCards, setAllCards] = useRecoilState<AllCardState>(AllCardStateAtom);
  const selectedColor = useRecoilValue<ManaColor | undefined>(SelectedColorStateAtom);
  const pagingCard = useRecoilValue<Array<Array<PossessionCard>>>(PagingCardSelector);
  const [colorLogo, setColorLogo] = useState<StaticImageData>(whiteLogo);
  const [bgColorClass, setBgColorClass] = useState<string>('bg-white');
  const [borderColorClass, setBorderColorClass] = useState<string>('border-gray-100');
  const [pagingCardList, setPagingCardList] = useState<Array<Array<PossessionCard>>>([]);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const init = useCallback(async () => {
    setLoading(true);
    const res = await fetch('/api/possessionCard');
    const data = await res.json();
    setAllCards({ acquired: true, data });
    setLoading(false);
  }, [setAllCards, setLoading]);

  useEffect(() => {
    !allCards.acquired && init();
  }, [init, allCards]);

  useEffect(() => {
    setTimeout(
      () => {
        setPagingCardList(pagingCard);
      },
      isOpen ? 1000 : 0,
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagingCard]);

  useEffect(() => {
    // 初期値: Color.MULTICOLOR
    let cardFileInfo: CardFileStyle = {
      colorLogo: multicolorLogo,
      bgColorClass: 'bg-gradient-to-br from-amber-600 to-red-500',
      borderColorClass: 'border-white',
    };
    switch (selectedColor) {
      case ManaColor.W:
        cardFileInfo = {
          colorLogo: whiteLogo,
          bgColorClass: 'bg-white',
          borderColorClass: 'border-gray-100',
        };
        break;
      case ManaColor.U:
        cardFileInfo = {
          colorLogo: blueLogo,
          bgColorClass: 'bg-blue-500',
          borderColorClass: 'border-blue-400',
        };
        break;
      case ManaColor.B:
        cardFileInfo = {
          colorLogo: blackLogo,
          bgColorClass: 'bg-black',
          borderColorClass: 'border-gray-900',
        };
        break;
      case ManaColor.R:
        cardFileInfo = {
          colorLogo: redLogo,
          bgColorClass: 'bg-red-500',
          borderColorClass: 'border-red-400',
        };
        break;
      case ManaColor.G:
        cardFileInfo = {
          colorLogo: greenLogo,
          bgColorClass: 'bg-green-500',
          borderColorClass: 'border-green-400',
        };
        break;
      case ManaColor.C:
        cardFileInfo = {
          colorLogo: colorlessLogo,
          bgColorClass: 'bg-gray-500',
          borderColorClass: 'border-gray-400',
        };
        break;
    }
    setTimeout(
      () => {
        setColorLogo(cardFileInfo.colorLogo);
        setBgColorClass(cardFileInfo.bgColorClass);
        setBorderColorClass(cardFileInfo.borderColorClass);
      },
      isOpen ? 1000 : 0,
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedColor]);

  return (
    <Presenter
      colorLogo={colorLogo}
      bgColorClass={bgColorClass}
      borderColorClass={borderColorClass}
      pagingCardList={pagingCardList}
      setIsOpen={setIsOpen}
    />
  );
};

export default CardFile;
