import React, { useRef, useState } from 'react';

import classnames from 'classnames';

export interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  label: string;
  setValue?: React.Dispatch<React.SetStateAction<string>>;
}

const Text: React.FC<InputProps> = ({ label, setValue, id, type, value, readOnly }) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);

  return (
    <div className={classnames('relative', 'bg-white')}>
      <input
        className={classnames(
          'w-full',
          'h-12',
          'px-4',
          'drop-shadow',
          'focus:outline-none',
          readOnly
            ? ['cursor-default', 'opacity-30']
            : ['focus:border-2', 'focus:border-amber-600', 'duration-300'],
        )}
        id={id}
        type={type}
        autoComplete="off"
        readOnly={readOnly}
        value={value}
        ref={inputRef}
        onChange={(e) => setValue && setValue(e.target.value)}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
      />
      <label
        htmlFor={id}
        className={classnames(
          'absolute',
          'left-2',
          'px-1',
          'font-bold',
          'bg-transparent',
          'transform',
          '-translate-y-1/2',
          'duration-300',
          !isFocus && !value
            ? ['top-1/2', 'text-sm', 'text-gray-400', 'cursor-text']
            : ['top-0', 'text-xs'],
        )}
        onClick={() => inputRef.current?.focus()}
      >
        {label}
      </label>
    </div>
  );
};

export default Text;
