import type { AppProps } from 'next/app';

import { RecoilRoot } from 'recoil';

import Layout from '@layout/Layout';
import '@styles/global.css';
import '@styles/tailwind.css';

const App = ({ Component, pageProps }: AppProps) => {
  const { title, statusCode } = pageProps;

  return (
    <RecoilRoot>
      {statusCode ? (
        <Component {...pageProps} />
      ) : (
        <Layout title={title}>
          <Component {...pageProps} />
        </Layout>
      )}
    </RecoilRoot>
  );
};

export default App;
