import { FC, useEffect, useState } from 'react';

import { useRecoilValue } from 'recoil';

import Presenter from './Presenter';
import SelectedColorStateAtom from '@recoil/atoms/SelectedColorStateAtom';
import { ManaColor } from '@types';

interface CoverEndProps {
  zIndex: number;
  bgColorClass: string;
  borderColorClass: string;
}

const CoverEnd: FC<CoverEndProps> = ({ zIndex, bgColorClass, borderColorClass }) => {
  const selectedColor = useRecoilValue<ManaColor | undefined>(SelectedColorStateAtom);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    setIsOpen(false);
  }, [selectedColor]);

  return (
    <Presenter
      isOpen={isOpen}
      setIsOpen={setIsOpen}
      zIndex={zIndex}
      bgColorClass={bgColorClass}
      borderColorClass={borderColorClass}
    />
  );
};

export default CoverEnd;
