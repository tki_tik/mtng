enum AtomKeys {
  ALL_CARD_STATE = 'allCardState',
  LOADING_STATE = 'loadingState',
  SELECTED_COLOR_STATE = 'selectedColorState',
  SHOW_TOAST_STATE = 'showToastState',
  SYMBOLOGY_STATE = 'symbologyState',
}

export default AtomKeys;
