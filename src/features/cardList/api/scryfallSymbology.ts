import axios from 'axios';

import { Symbology } from '@types';

const URL = `${process.env.NEXT_PUBLIC_SCRYFALL_DOMAIN}/symbology`;

export interface ScryfallSymbologyResponse {
  object: string;
  has_more: boolean;
  data: Array<Symbology>;
}

const scryfallSymbology = async (): Promise<ScryfallSymbologyResponse> => {
  const res = await axios.get<ScryfallSymbologyResponse>(URL);
  return res.data;
};

export default scryfallSymbology;
