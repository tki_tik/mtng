import { FC, ReactNode } from 'react';

import classnames from 'classnames';

interface Props {
  isOpen: boolean;
  coverColorClass?: string;
  children: ReactNode;
}

const Presenter: FC<Props> = ({ isOpen, coverColorClass, children }) => {
  return (
    <div
      className={classnames(
        'absolute',
        'right-1/2',
        'w-1/2',
        'h-full',
        'p-3',
        'rounded-l-lg',
        'transition-transform',
        'duration-1000',
        'origin-right',
        'backface-hidden',
        coverColorClass ?? 'bg-gray-950',
      )}
      style={{
        transform: `perspective(1500px) rotateY(${isOpen ? 0 : 180}deg`,
      }}
    >
      {children}
    </div>
  );
};

export default Presenter;
