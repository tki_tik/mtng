import { StaticImageData } from 'next/image';
import { Dispatch, FC, SetStateAction } from 'react';

import classnames from 'classnames';

import CoverEnd from '@features/cardList/components/CoverEnd';
import CoverFront from '@features/cardList/components/CoverFront';
import FilePage from '@features/cardList/components/FilePage';
import { PossessionCard } from '@types';

interface Props {
  colorLogo: StaticImageData;
  bgColorClass: string;
  borderColorClass: string;
  pagingCardList: Array<Array<PossessionCard>>;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
}

const Presenter: FC<Props> = ({
  colorLogo,
  bgColorClass,
  borderColorClass,
  pagingCardList,
  setIsOpen,
}) => {
  return (
    <div className={classnames('relative', 'h-full')} style={{ aspectRatio: '16/10' }}>
      <CoverFront
        zIndex={pagingCardList.length + 12}
        colorLogo={colorLogo}
        bgColorClass={bgColorClass}
        borderColorClass={borderColorClass}
        setIsCardFileOpen={setIsOpen}
      />
      {pagingCardList.map((pagingCard, i) => (
        <FilePage zIndex={pagingCardList.length + 11 - i} cardList={pagingCard} key={i} />
      ))}
      <CoverEnd zIndex={10} bgColorClass={bgColorClass} borderColorClass={borderColorClass} />
    </div>
  );
};

export default Presenter;
