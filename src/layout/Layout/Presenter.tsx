import Head from 'next/head';
import { FC, ReactNode } from 'react';

import classnames from 'classnames';

import Loading from '@components/Loading';
import Toast, { ToastInfo } from '@components/Toast';
import Sidebar from '@layout/Sidebar';
import { MessageLevel } from '@types';

interface Props {
  loading: boolean;
  toastInfo: ToastInfo;
  title: string;
  children: ReactNode;
}

const Presenter: FC<Props> = ({ loading, toastInfo, title, children }) => {
  return (
    <>
      <Head>
        <meta name="description" content="mtg card management" />
        <link rel="icon" href="/favicon.ico" />
        <title>{title}</title>
      </Head>
      <div
        className={classnames('bg-gradient-to-br', 'from-black', 'via-red-950', 'to-yellow-800')}
      >
        <Sidebar />
        <main
          className={classnames('flex', 'justify-center', 'w-screen', 'h-screen', 'p-4', 'pl-28')}
        >
          {children}
        </main>
      </div>
      {loading && <Loading />}
      {toastInfo.level !== MessageLevel.NONE && <Toast />}
    </>
  );
};

export default Presenter;
