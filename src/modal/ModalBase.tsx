import { FC, ReactNode } from 'react';

import classnames from 'classnames';
import { MdClose } from 'react-icons/md';
import Modal from 'react-modal';

interface ModalBaseProps {
  isOpen: boolean;
  onClose: () => void;
  children: ReactNode;
}

const ModalBase: FC<ModalBaseProps> = ({ isOpen, onClose, children }) => {
  return (
    <Modal
      className={classnames(
        'absolute',
        'top-1/2',
        'left-1/2',
        'transform',
        'translate-x-[calc(-50%+40px)]',
        '-translate-y-1/2',
        'outline-none',
      )}
      overlayClassName={classnames(
        'fixed',
        'w-screen',
        'h-screen',
        'top-0',
        'left-0',
        'bg-opacity-80',
        'bg-black',
        'z-50',
      )}
      ariaHideApp={false}
      isOpen={isOpen}
      onRequestClose={onClose}
    >
      <MdClose
        className={classnames('absolute', 'top-4', 'right-4', 'cursor-pointer')}
        size={24}
        onClick={onClose}
      />
      {children}
    </Modal>
  );
};

export default ModalBase;
