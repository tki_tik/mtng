import { atom } from 'recoil';

import AtomKeys from '../AtomKeys';
import { PossessionCard } from '@types';

export interface AllCardState {
  acquired: boolean;
  data: Array<PossessionCard>;
}

const AllCardStateAtom = atom<AllCardState>({
  key: AtomKeys.ALL_CARD_STATE,
  default: { acquired: false, data: [] },
});

export default AllCardStateAtom;
