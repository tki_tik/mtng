import axios from 'axios';

import { Card } from '@features/addCard/types';

const URL = `${process.env.NEXT_PUBLIC_SCRYFALL_DOMAIN}/cards`;

interface ScryfallCardsCodeNumberLangRequest {
  set: string;
  lang: string;
  number: string;
}

export interface ScryfallCardsCodeNumberLangResponse extends Card {
  object: string;
  total_cards: number;
  has_more: boolean;
}

const scryfallCardsCodeNumberLang = async (
  req: ScryfallCardsCodeNumberLangRequest,
): Promise<ScryfallCardsCodeNumberLangResponse> => {
  const url = `${URL}/${req.set}/${req.number}/${req.lang}`;
  const res = await axios.get<ScryfallCardsCodeNumberLangResponse>(url, {
    validateStatus: (status) => status >= 200 && status < 500,
  });
  return res.data;
};

export default scryfallCardsCodeNumberLang;
