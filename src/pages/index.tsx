import { GetStaticProps, NextPage } from 'next';
import { useEffect } from 'react';

import classnames from 'classnames';
import { useRecoilState } from 'recoil';

import scryfallSymbology from '@features/cardList/api/scryfallSymbology';
import CardFile from '@features/cardList/components/CardFile';
import SelectColor from '@features/cardList/components/SelectColor';
import SymbologyStateAtom from '@recoil/atoms/SymbologyStateAtom';
import { Symbology } from '@types';

interface PageProps {
  symbology: Array<Symbology>;
}

const Home: NextPage<PageProps> = (props) => {
  const [symbology, setSymbology] = useRecoilState<Array<Symbology>>(SymbologyStateAtom);

  useEffect(() => {
    symbology.length === 0 && setSymbology(props.symbology);
  }, [props.symbology, setSymbology, symbology]);

  return (
    <div className={classnames('flex', 'flex-col', 'gap-4', 'w-full', 'h-full')}>
      <SelectColor />
      <div className={classnames('flex', 'justify-center', 'flex-grow', 'h-0')}>
        <CardFile />
      </div>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const { data: symbology } = await scryfallSymbology();

  return {
    props: {
      title: 'カード一覧',
      symbology,
    },
  };
};

export default Home;
