import { FC, ReactNode } from 'react';

import classnames from 'classnames';

interface Props {
  isOpen: boolean;
  coverColorClass?: string;
  zIndex: number;
  children: ReactNode;
}

const Presenter: FC<Props> = ({ isOpen, coverColorClass, zIndex, children }) => {
  return (
    <div
      className={classnames(
        'absolute',
        'left-1/2',
        'w-1/2',
        'h-full',
        'p-3',
        'rounded-r-lg',
        'transition-transform',
        'duration-1000',
        'origin-left',
        'backface-hidden',
        coverColorClass ?? 'bg-gray-950',
      )}
      style={{
        transform: `perspective(1500px) rotateY(${isOpen ? -180 : 0}deg`,
        zIndex: zIndex,
      }}
    >
      {children}
    </div>
  );
};

export default Presenter;
