import { Dispatch, FC, MouseEvent, SetStateAction } from 'react';

import classnames from 'classnames';

import Image from '@components/Image';
import { FOIL_TYPE } from '@constants';
import CardInfoModal from '@modal/CardInfoModal';
import { PossessionCard } from '@types';

interface Props {
  cardList: Array<PossessionCard>;
  isDispCardInfoModal: boolean;
  setIsDispCardInfoModal: Dispatch<SetStateAction<boolean>>;
  activeCard?: PossessionCard;
  onCardClick: (e: MouseEvent<HTMLDivElement, globalThis.MouseEvent>, index: number) => void;
}

const Presenter: FC<Props> = ({
  cardList,
  isDispCardInfoModal,
  setIsDispCardInfoModal,
  activeCard,
  onCardClick,
}) => {
  return (
    <div
      className={classnames(
        'grid',
        'grid-cols-3',
        'grid-rows-3',
        'w-full',
        'h-full',
        'border',
        'border-gray-900',
        'border-dashed',
      )}
    >
      {cardList.map(({ imageUrl, number, foilType }, i) => (
        <div
          className={classnames(
            'relative',
            'flex',
            'justify-center',
            'items-center',
            'p-1',
            'border',
            'border-gray-900',
            'border-dashed',
          )}
          key={i}
        >
          {imageUrl && (
            <div
              className={classnames(
                'relative',
                'w-full',
                'h-full',
                'hover:-translate-y-10',
                'hover:cursor-pointer',
                'hover:duration-500',
                Number(foilType) && [
                  foilType !== '1' && 'border-amber-600',
                  'border-2',
                  'rounded-lg',
                ],
              )}
              onClick={(e) => onCardClick(e, i)}
            >
              <Image type={'image/jpeg'} src={imageUrl} alt={'card image'} objectContain={false} />
              <div
                className={classnames(
                  'absolute',
                  'left-1',
                  'bottom-1',
                  'max-w-[calc(100%-48px)]',
                  'hover:max-w-full',
                  Number(foilType) && [
                    'px-2',
                    'py-1',
                    'border',
                    'border-white',
                    'rounded',
                    'bg-black',
                  ],
                )}
              >
                {foilType !== '0' && (
                  <p
                    className={classnames(
                      foilType !== '1' ? 'text-amber-600' : 'text-white',
                      'text-xs',
                      'font-bold',
                      'truncate',
                    )}
                  >
                    {Object.values(FOIL_TYPE)[Number(foilType)]}
                  </p>
                )}
              </div>
              <div
                className={classnames(
                  'absolute',
                  'right-1',
                  'bottom-1',
                  'px-2',
                  'py-1',
                  'border',
                  'border-white',
                  'rounded',
                  'bg-black',
                )}
              >
                <p className={classnames('text-white', 'text-xs', 'font-bold')}>{`x${number}`}</p>
              </div>
            </div>
          )}
          <div
            className={classnames(
              'absolute',
              'bottom-0',
              'w-full',
              'h-2/3',
              'bg-white',
              'bg-opacity-10',
            )}
          />
        </div>
      ))}
      {activeCard && (
        <CardInfoModal
          card={activeCard}
          isOpen={isDispCardInfoModal}
          onClose={() => setIsDispCardInfoModal(false)}
        />
      )}
    </div>
  );
};

export default Presenter;
