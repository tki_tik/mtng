import { Legality } from '@features/addCard/types';
import { PossessionCard } from '@types';

export const INITIAL_POSSESSION_CARD: PossessionCard = {
  multiverseid: 0,
  lang: '',
  name: '',
  originalName: '',
  imageUrl: '',
  set: '',
  setNumber: 0,
  colorIdentity: [],
  manaCost: '',
  cmc: 0,
  rarity: '',
  type: '',
  originalType: '',
  text: '',
  legalities: {
    standard: Legality.legal,
    future: Legality.legal,
    historic: Legality.legal,
    gladiator: Legality.legal,
    pioneer: Legality.legal,
    explorer: Legality.legal,
    modern: Legality.legal,
    legacy: Legality.legal,
    pauper: Legality.legal,
    vintage: Legality.legal,
    penny: Legality.legal,
    commander: Legality.legal,
    oathbreaker: Legality.legal,
    brawl: Legality.legal,
    historicbrawl: Legality.legal,
    alchemy: Legality.legal,
    paupercommander: Legality.legal,
    duel: Legality.legal,
    oldschool: Legality.legal,
    premodern: Legality.legal,
    predh: Legality.legal,
  },
  foilType: '',
  number: '',
};

export const LANG_TYPE: { [key: string]: string } = {
  JP: 'ja',
  EN: 'en',
};

export const FOIL_TYPE = {
  NORMAL: '通常',
  PICTURE_DIFFERENCE: '絵違い',
  FOIL: 'Foil',
  PICTURE_DIFFERENCE_FOIL: '絵違いFoil',
  ETCHING_FOIL: 'エッチングFoil',
  TEXTURE_FOIL: 'テクスチャーFoil',
  STEP_COMPLETE_FOIL: 'S&C F',
};

export enum CardType {
  'Planeswalker',
  'Creature',
  'Artifact',
  'Conspiracy',
  'Enchantment',
  'Instant',
  'Sorcery',
  'Land',
  /** 以下見たことのないCardType */
  'Phenomenon',
  'Plane',
  'Scheme',
  'Vanguard',
}
