import { Dispatch, FC, OptionHTMLAttributes, SelectHTMLAttributes, SetStateAction } from 'react';

import classnames from 'classnames';
import { MdKeyboardArrowDown } from 'react-icons/md';

export interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  blankOptionLabel?: string;
  optionList: Array<OptionHTMLAttributes<HTMLOptionElement>>;
  setValue?: Dispatch<SetStateAction<string>>;
}

const Select: FC<SelectProps> = ({
  blankOptionLabel,
  optionList,
  setValue,
  value,
  onChange,
  disabled,
}) => {
  return (
    <div className={classnames('flex', 'relative', 'h-full', 'z-10', 'shadow')}>
      <select
        className={classnames(
          'pl-3',
          'pr-12',
          'text-sm',
          'bg-transparent',
          'z-10',
          'appearance-none',
          'focus: outline-none',
          disabled ? ['cursor-default', 'opacity-30'] : ['cursor-pointer'],
        )}
        value={value}
        onChange={onChange ? onChange : (e) => setValue && setValue(e.target.value)}
        disabled={disabled}
      >
        {blankOptionLabel && <option value={''}>{blankOptionLabel}</option>}
        {optionList.map((option, i) => (
          <option {...option} key={i} />
        ))}
      </select>
      <MdKeyboardArrowDown
        className={classnames(
          'absolute',
          'transform',
          '-translate-y-1/2',
          'top-1/2',
          'right-1',
          disabled && 'opacity-30',
        )}
        size={24}
      />
    </div>
  );
};

export default Select;
